# Trees and Graphs: Is it balanced?
<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

The definition of a balanced binary tree:<br>
&emsp;\- The difference between the height of the left subtree and the height of the right subtree is at most 1<br>
&emsp;\- The right subtree is balanced<br>
&emsp;\- The left subtree is balanced<br>

<b>`Hint:`&nbsp;Starting from the root node, traverse through the tree.  Stop at every node and check if the current node satisfies above rules.<br>

<b>`Note:`</b>&nbsp;To calculate the height of a node, you can use the method you implemented in "How Tall is the Tree?" challenge.<br>
<b>`Note:`</b>&nbsp;As you can see in the above definition, three sub-problems are already defined. If you can define sub-problems strictly, then there is an easy recursive solution.
 
</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;How to check if a node is balanced</summary><br>

Take a look at algorithm flow illustrated below.

<img src="../../images/trees_and_graphs/is-it-balanced/is-it-balanced-1.png"  width="800"><br>
<em>Each block that is surrounded by a square, represents a recursive step.</em>

<b>`Hint:`&nbsp;First calculate the  height of left and right subtree. Difference between them must be lesser than 1.<br>
<b>`Hint:`&nbsp;The left and right subtree must meet the above requirement as well.<br>
  
</details>
<!-- Hint 2 -->

---

<!-- Solution 1 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Recursive)</summary><br>

```ruby
def balanced_tree? (array)
  root = array_to_tree(array, 0)
  balanced?(root)
end

def balanced? (node)
  return true if node.nil?
  # calculate left subtree's height
  left_height = height(node.left)
  # calculate right subtree's height
  right_height = height(node.right)
  # if the difference between right and left height greater than 1, it's imbalanced
  return false if (left_height - right_height).abs > 1
  # Are the left and right subtree balanced?
  balanced?(node.left) && balanced?(node.right)
end

# calculates the height of the given node
def height(node)
  # use your method from the previous challenge 
end
```
  
</details>
<!-- Solution 1 -->

---

<!-- Solution 2 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Iterative) - only for curious 😎</summary><br>

```ruby
def balanced_tree? (array)
  root = array_to_tree(array, 0)
  stack = [root]
  until stack.empty?
    current = stack.pop
    left_height = height(current.left)
    right_height = height(current.right)
    return false if (left_height - right_height).abs > 1

    stack.push(current.right) unless current.right.nil?
    stack.push(current.left) unless current.left.nil?
  end
  true
end
```

</details>
<!-- Solution 2 -->
