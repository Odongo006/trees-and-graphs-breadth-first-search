# Sorting Algorithms: Quick Sort Running Time

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

In this challenge, we will implement a basic counter to count total swaps. Then we will compare the quick-sort and the insertion-sort algorithms we previously solved.<br>
Before comparing these two algorithms, make sure that you have implemented them correctly. Otherwise the comparison will be incorrect.

We will leverage two Ruby utilities to solve the challenge in the simplest manner.

First, we use global Ruby variables.

You can define global variables by using the dollar($) sign before the variable name. Then you can access or mutate it anywhere you want:<br>
```ruby
$global_variable = 5
local_variable = 5

function mutate
  $global_variable = 0
  local_variable = 0
end

puts $global_variable
=> 5
puts local_variable
=> 5

mutate

# the global variable changed
puts $global_variable
=> 0
# the local variable didn't change
puts local_variable
=> 5
```

We use in-place algorithms at each solution. That is, we mutate the given array every time.

You cannot call these two solutions sequentally, since the first sorting function will mutate the array, resulting in the array already being sorted for the second call:
```ruby
# array = [3, 2, 1]
def quicksort_running_time(array)
  # at this point, array = [3, 2, 1]
  advanced_quicksort(array)
  # at this point, array = [1, 2, 3]
  # so, you cannot do it this way
  insertion_sort(array)
end
```

Second, for a quick solution to prevent mutation, we use array clones.

```ruby
def quicksort_running_time(array)
  # at this point, array = [3, 2, 1]
  advanced_quicksort(array.clone)
  # at this point, array = [3, 2, 1]
  # because we mutated just the clone array
  insertion_sort(array.clone)
end
```

<b>`Hint:`&nbsp;Define two global counter variables to count the swaps in each algorithm.<br>
<b>`Hint:`&nbsp;Increment the counter at each swap.<br>
<b>`Hint:`&nbsp;The solution is just the difference between the two global variables.<br>
  
<b>`Note:`</b>&nbsp;You have to reset the global counters at each test case.<br>

</details>
<!-- Hint 1 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution</summary><br>

```ruby
# insertion sort
def insertion_sort(array)
  for i in 1...(array.length)
    current = array[i]
    j = i - 1
    while j >= 0 && current < array[j]
      array[j + 1] = array[j]
      # increment the counter after the swap
      $insertion_sort_counter += 1
      j -= 1
    end
    array[j + 1] = current
  end
end

# quick sort
def advanced_quicksort(array, begin_index = 0, end_index = array.length - 1)
  if begin_index < end_index
    pivot = partition(array, begin_index, end_index)
    advanced_quicksort(array, begin_index, pivot - 1)
    advanced_quicksort(array, pivot + 1, end_index)
  end
end

def partition(array, begin_index, end_index)
  pivot = array[end_index]
  j = begin_index
  for i in begin_index...end_index
    if array[i] < pivot
      array[i], array[j] = array[j], array[i]
      # increment the counter after the swap
      $quick_sort_counter += 1
      j += 1
    end
  end
  # increment the counter after the swap
  array[end_index], array[j] = array[j], array[end_index]
  $quick_sort_counter += 1
  j
end

def quicksort_running_time(array)
  # define globals here to reset their values at each test case
  $quick_sort_counter = 0
  $insertion_sort_counter = 0
  # call an insertion sort for the clone array
  insertion_sort(array.clone)
  # call a quick sort for the clone array
  advanced_quicksort(array.clone)

  $insertion_sort_counter - $quick_sort_counter
end
```
  
</details>
<!-- Hint end-->

